# Solution to Optimax Energy test task 
## Task definition
A product x QU (quantity units) will be auctioned under 2 parties. The parties have each y MU (monetary units) for auction. They offer then simultaneously an arbitrary number of its MU on the first 2 QU of the product. After that, the bids will be visible to both. The 2 QU of the product is awarded to who has offered the most MU; if both bid the same, then both get 1 QU. Both bidders must pay their amount - including the defeated. A bid of 0 MU is allowed. Bidding on each 2 QU is repeated until the supply of x QU is fully auctioned. Each bidder aims to get a larger amount than its competitor.

In an auction wins the program that is able to get more QU than the other. With a tie, the program that retains more MU wins. Write a program that can participate in such an auction and competes with one of our programs. Please explain its strategy.

## Instruction to startup
1. Execute ```mvn clean package```
1. Run application by ```mvn exec:java -Dexec.mainClass=com.github.usmanovbf.optimax.AuctionStartup -Dexec.args="-mu 80 -qu 15"```
