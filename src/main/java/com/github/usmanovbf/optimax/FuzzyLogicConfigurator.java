package com.github.usmanovbf.optimax;

import com.fuzzylite.Engine;
import com.fuzzylite.activation.General;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.norm.s.AlgebraicSum;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.AlgebraicProduct;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Gaussian;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

/**
 * Prepare fuzzy logic controller
 */
public class FuzzyLogicConfigurator {

    /**
     * Configure possible situations and behaviour of our bidder
     */
    public RuleBlock prepareRules(Engine engine) {
        RuleBlock ruleBlock = new RuleBlock();

        ruleBlock.setEnabled(true);

        ruleBlock.setActivation(new General());
        ruleBlock.setDisjunction(new AlgebraicSum());
        ruleBlock.setConjunction(new AlgebraicProduct());
        ruleBlock.setImplication(new Minimum());

        // Declare different situations with readable language
        ruleBlock.addRule(
                Rule.parse("if AVAILABLE_MONEY is TIGHT and DEVIATION is LOW then ACTION is BUY_WITH_LOW_PRICE",
                           engine));
        ruleBlock.addRule(
                Rule.parse("if AVAILABLE_MONEY is TIGHT and DEVIATION is MEDIUM then ACTION is SKIP",
                           engine));
        ruleBlock.addRule(
                Rule.parse("if AVAILABLE_MONEY is TIGHT and DEVIATION is HIGH then ACTION is SKIP",
                           engine));
        ruleBlock.addRule(
                Rule.parse("if AVAILABLE_MONEY is ENOUGH and DEVIATION is LOW then ACTION is BUY_WITH_LOW_PRICE",
                           engine));
        ruleBlock.addRule(
                Rule.parse(
                        "if AVAILABLE_MONEY is ENOUGH and DEVIATION is MEDIUM then ACTION is BUY_WITH_STANDARD_PRICE",
                        engine));
        ruleBlock.addRule(
                Rule.parse("if AVAILABLE_MONEY is ENOUGH and DEVIATION is HIGH then ACTION is BUY_WITH_STANDARD_PRICE",
                           engine));

        return ruleBlock;
    }

    /**
     * Keep in memory deviation between last single ourBid of winner on each step
     * and best price for one lot
     */
    public InputVariable prepareDeviation(double bestPrice) {
        InputVariable deviation = new InputVariable();
        deviation.setName("DEVIATION");
        deviation.setEnabled(true);
        deviation.setRange(0.000, bestPrice * 3);
        deviation.setLockValueInRange(true);
        deviation.addTerm(new Trapezoid("LOW", 0.000, 0.000, bestPrice / 2, bestPrice));
        deviation.addTerm(new Gaussian("MEDIUM", bestPrice, bestPrice / 10));
        deviation.addTerm(new Trapezoid("HIGH", bestPrice, bestPrice + bestPrice / 2, bestPrice * 3,
                                        bestPrice * 3));
        return deviation;
    }

    /**
     * Make influence on our strategy which depends on amount of available money
     */
    public InputVariable prepareAvailableMoney(double availableMoney) {
        InputVariable budgetVar = new InputVariable();
        budgetVar.setName("AVAILABLE_MONEY");
        budgetVar.setEnabled(true);
        budgetVar.setRange(0.000, availableMoney);
        budgetVar.setLockValueInRange(true);
        budgetVar.addTerm(new Trapezoid("TIGHT", 0.000, 0.0, availableMoney / 3, availableMoney / 1.5));
        budgetVar.addTerm(
                new Trapezoid("ENOUGH", availableMoney / 4, availableMoney / 1.5, availableMoney, availableMoney));
        return budgetVar;
    }

    /**
     * Configure possible actions in advance
     */
    public OutputVariable prepareAction(double bestPriceForSingle) {
        OutputVariable action = new OutputVariable();
        action.setName("ACTION");
        action.setEnabled(true);
        action.setRange(0.000, bestPriceForSingle * 3);
        action.setLockValueInRange(false);
        action.setAggregation(new Maximum());
        action.setDefuzzifier(new Centroid(100));
        action.setDefaultValue(bestPriceForSingle);
        action.setLockPreviousValue(false);

        action.addTerm(new Trapezoid("BUY_WITH_LOW_PRICE", 0.000, 0.000, bestPriceForSingle / 2, bestPriceForSingle));
        action.addTerm(new Gaussian("BUY_WITH_STANDARD_PRICE", bestPriceForSingle, bestPriceForSingle / 10));
        action.addTerm(new Trapezoid("SKIP", bestPriceForSingle, bestPriceForSingle + bestPriceForSingle / 2,
                                     bestPriceForSingle * 3,
                                     bestPriceForSingle * 3));

        return action;
    }
}