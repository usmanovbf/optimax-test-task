package com.github.usmanovbf.optimax;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initial class for emulating auction
 */
public class AuctionStartup {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuctionStartup.class);

    /** Amount of monetary units on startup **/
    private static int INITIAL_MU;

    /** Amount of quantity units on startup **/
    private static int INITIAL_QU;

    public static void main(String[] args) throws ParseException {
        extractCliArguments(args);
        LOGGER.debug("CLI arguments extracted: {}, {}", INITIAL_MU, INITIAL_QU);

        Bidder bidder = new BidderImpl();
        bidder.init(INITIAL_QU, INITIAL_MU);
        LOGGER.debug("Bidder initialized with arguments: {}, {}", INITIAL_MU, INITIAL_QU);

        AuctionManager auctionManager = new AuctionManager(bidder, INITIAL_MU, INITIAL_QU);
        auctionManager.startAuctionManagement();
    }

    /**
     * Extracting command line arguments like
     * initial monetary units and quantity units
     *
     * @param args arguments from command line
     */
    private static void extractCliArguments(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        // Prepare options
        Options options = new Options();
        Option muOption = new Option("mu",
                                     "monetary-units",
                                     true,
                                     "The monetary units that each side has");
        options.addOption(muOption);
        Option quOption = new Option("qu",
                                     "quantity-units",
                                     true,
                                     "The quantity units that will be auctioned under 2 parties");
        options.addOption(quOption);

        // Extract options
        try {
            CommandLine commandLine = parser.parse(options, args);

            String muValue = commandLine.getOptionValue(muOption.getOpt());
            String quValue = commandLine.getOptionValue(quOption.getOpt());

            if (muValue == null || quValue == null) {
                HelpFormatter helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("ant", options);
                throw new IllegalArgumentException("Both variables must be.");
            }
            INITIAL_MU = Integer.valueOf(muValue);
            INITIAL_QU = Integer.valueOf(quValue);

        } catch (NumberFormatException e) {
            String messagePrefix = "Error while converting values to integer type: ";
            LOGGER.error(messagePrefix + e.getMessage(), e.getCause());
            throw e;
        } catch (ParseException e) {
            String messagePrefix = "Error occurred while parsing command line arguments: ";
            LOGGER.error(messagePrefix + e.getMessage(), e.getCause());
            throw e;
        }

    }
}
