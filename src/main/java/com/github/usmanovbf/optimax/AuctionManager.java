package com.github.usmanovbf.optimax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Scanner;

/**
 * That side, who manages an auction and control following the rules
 */
public class AuctionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuctionManager.class);

    /** Bidder who will compete with external bidder **/
    private Bidder ourBidder;

    /** Amount of monetary units of our bidder at this moment **/
    private int ourCurrentMU;

    /** Amount of quantity units of our bidder at this moment **/
    private int ourCurrentQU;

    /** Amount of monetary units of external bidder at this moment **/
    private int externalCurrentMU;

    /** Amount of quantity units of external bidder at this moment **/
    private int externalCurrentQU;

    /** Amount of quantity units at this moment **/
    private int currentQU;

    public final static String OUR_BIDDER_WINNER_MESSAGE = "Our bidder is the winner!";

    public final static String USER_DECIDED_TO_QUIT = "User decided to quit.";

    public AuctionManager(Bidder bidder, int initialMU, int initialQU) {
        this.ourBidder = bidder;
        this.ourCurrentMU = initialMU;
        this.externalCurrentMU = initialMU;
        this.currentQU = initialQU;
    }

    /**
     * Manages the auction
     */
    public void startAuctionManagement() {
        int externalBid;
        int step = 0;
        while (isItPossibleToContinue()) {
            LOGGER.debug("Current step is {}", step++);

            int ourBid = ourBidder.placeBid();
            // If we decided to get more ourBid than we have in our pocket
            if (ourCurrentMU - ourBid < 0) {
                ourBid = ourCurrentMU;
            }
            System.out.println(MessageFormat.format("Our ourBid is {0}. Enter your ourBid or \"quit\": ", ourBid));

            try {
                externalBid = readExternalBid();
            } catch (IllegalStateException e) {
                System.out.println(USER_DECIDED_TO_QUIT);
                chooseWinnerAndExit();
                break;
            }
            System.out.println("External ourBid is " + externalBid);
            ourBidder.bids(ourBid, externalBid);

            // To prevent negative balance of monetary units
            if (externalCurrentMU - externalBid < 0) {
                System.out.println("Your ourBid is higher than it is possible. Please enter again.");
                continue;
            }
            // If our bidder won current step
            if (ourBid > externalBid) {
                ourCurrentMU -= ourBid;
                ourCurrentQU += 2;

                externalCurrentMU -= externalBid;
            }
            // If external bidder won current step
            else if (ourBid < externalBid) {
                ourCurrentMU -= ourBid;

                externalCurrentMU -= externalBid;
                externalCurrentQU += 2;
            }
            // If it is draw on current step
            else if (ourBid == externalBid) {
                ourCurrentMU -= ourBid;
                ourCurrentQU += 1;

                externalCurrentMU -= externalBid;
                externalCurrentQU += 1;
            }
            currentQU -= 2;
        }
    }

    /**
     * Check different possibilities to continue the auction.
     * If it is not possible, print a reason and sum up the results
     *
     * @return true if it is possible to continue the auction
     */
    private boolean isItPossibleToContinue() {
        if (currentQU == 1) {
            System.out.println("Remaining amount of quantity units is 1. There is no way to buy only one unit.");
            chooseWinnerAndExit();
            return false;
        }
        if (currentQU == 0) {
            System.out.println("All quantity units are over.");
            chooseWinnerAndExit();
            return false;
        }
        if (ourCurrentMU == 0) {
            System.out.println("The money of our bidder are over.");
            chooseWinnerAndExit();
            return false;
        }
        if (externalCurrentMU == 0) {
            System.out.println("The money of external bidder are over.");
            chooseWinnerAndExit();
            return false;
        }

        return true;
    }

    /**
     * Receive external ourBid from CLI
     * and prevent invalid formats
     *
     * @return typed external ourBid
     */
    private int readExternalBid() {
        int externalBid;
        Scanner scanner = new Scanner(System.in);
        String next = null;
        while (true) {
            try {
                if (scanner.hasNext()) {
                    next = scanner.next();
                }
                if (next.equals("quit")) {
                    throw new IllegalStateException();
                }
                externalBid = Integer.parseInt(next);
                break;
            } catch (NumberFormatException e) {
                LOGGER.warn("Invalid ourBid, try again: {}", e.getMessage());
            }
        }
        return externalBid;
    }

    /**
     * Sum up the results and print them
     */
    private void chooseWinnerAndExit() {
        System.out.println("--------------------");
        System.out.println("The auction is finished.");

        // Our bidder will win if he will have more quantity units OR during a draw more monetary units will have
        if (ourCurrentQU > externalCurrentQU
                || (ourCurrentQU == externalCurrentQU && ourCurrentMU > externalCurrentMU)) {
            System.out.println(OUR_BIDDER_WINNER_MESSAGE);
        }
        // External bidder will win if he will have more quantity units OR during a draw more monetary units will have
        else if (ourCurrentQU < externalCurrentQU || (ourCurrentQU == externalCurrentQU
                && ourCurrentMU < externalCurrentMU)) {
            System.out.println("External bidder is the winner!");
        }
        // If amount of monetary units and quantity units are same on both sides
        if (ourCurrentQU == externalCurrentQU) {
            System.out.println("It's a draw! Friendship won :-) ");
        }

        System.out.println("Our Current MU is " + ourCurrentMU);
        System.out.println("Our Current QU is " + ourCurrentQU);
        System.out.println("External Current MU is " + externalCurrentMU);
        System.out.println("External Current QU is " + externalCurrentQU);
        System.out.println("Current QU is " + currentQU);

    }
}