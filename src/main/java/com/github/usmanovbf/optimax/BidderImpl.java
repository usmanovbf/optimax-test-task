package com.github.usmanovbf.optimax;

import com.fuzzylite.Engine;
import com.fuzzylite.variable.InputVariable;

/**
 * {@inheritDoc}
 */
public class BidderImpl implements Bidder {

    /** Ideal price on each step **/
    private int bestPriceForSingle;

    /** Engine of fuzzy logic **/
    private Engine engine;

    private int initialMonetaryUnits;

    private int bidOfLastWinner;

    /** Total amount of expenses at the moment **/
    private int expenses;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(int quantity, int cash) {
        initialMonetaryUnits = cash;

        bestPriceForSingle = cash / quantity;

        engine = new Engine();

        //Confifgure Fuzzy Logic Controller
        FuzzyLogicConfigurator configurator = new FuzzyLogicConfigurator();
        engine.addInputVariable(configurator.prepareAvailableMoney(cash));
        engine.addInputVariable(configurator.prepareDeviation(bestPriceForSingle));

        engine.addOutputVariable(configurator.prepareAction(bestPriceForSingle));

        engine.addRuleBlock(configurator.prepareRules(engine));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int placeBid() {

        // Pass to strategy builder engine amount of available money
        InputVariable availableMoney = engine.getInputVariable("AVAILABLE_MONEY");
        availableMoney.setValue(initialMonetaryUnits - expenses);

        // Pass to strategy builder engine deviation
        InputVariable deviation = engine.getInputVariable("DEVIATION");
        deviation.setValue(Math.abs(bidOfLastWinner - bestPriceForSingle));

        engine.process();

        return (int) Math.round(engine.getOutputValue("ACTION"));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void bids(int own, int other) {
        // Keep in memory resultOfStep of current step for building a strategy on next step
        if (own > other) {
            changeState(new ResultState(own, other, ResultState.RESULT_OF_STEP.WIN));
        } else if (own < other) {
            changeState(new ResultState(own, other, ResultState.RESULT_OF_STEP.LOST));
        } else {
            changeState(new ResultState(own, other, ResultState.RESULT_OF_STEP.DRAW));
        }
    }

    /**
     * DTO for describing current step
     */
    private static class ResultState {

        // Do our bidder wins on this step?
        public enum RESULT_OF_STEP {
            LOST, DRAW, WIN
        }

        int ourBid;

        int otherBid;

        RESULT_OF_STEP resultOfStep;

        public ResultState(int ourBid, int externalBid, RESULT_OF_STEP resultOfStep) {
            this.ourBid = ourBid;
            this.otherBid = externalBid;
            this.resultOfStep = resultOfStep;
        }
    }

    /**
     * Keep in memory resultOfStep of current step
     */
    private void changeState(ResultState resultState) {
        this.expenses += resultState.ourBid;

        switch (resultState.resultOfStep) {
        case LOST:
            bidOfLastWinner = resultState.otherBid;
            break;
        case DRAW:
            bidOfLastWinner = resultState.ourBid;
            break;
        case WIN:
            bidOfLastWinner = resultState.ourBid;
        }

    }

}