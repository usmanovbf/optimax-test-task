package com.github.usmanovbf.optimax;

import org.junit.Test;

import java.text.ParseException;

/**
 * Test application CLI options
 */
public class AuctionStartupTest {

    @Test(expected = IllegalArgumentException.class)
    public void testBlankArgs() throws Exception {
        AuctionStartup auctionStartup = new AuctionStartup();
        auctionStartup.main(new String[] {});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSingleAmountArgs() throws Exception {
        AuctionStartup auctionStartup = new AuctionStartup();
        auctionStartup.main(new String[] {"-mu", "123"});
    }
    @Test(expected = NumberFormatException.class)
    public void testInvalidArgs() throws Exception {
        AuctionStartup auctionStartup = new AuctionStartup();
        auctionStartup.main(new String[] {"-mu", "1233", "-qu", "blabla"});
    }
}