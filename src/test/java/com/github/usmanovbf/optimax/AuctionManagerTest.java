package com.github.usmanovbf.optimax;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Random;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.doReturn;

/**
 * Test of auction with random external bids
 * It might be very slow, because random values does not relate to inner state to auction
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AuctionManager.class)
public class AuctionManagerTest {

    /**
     * Test auction with random external bids.
     * Check non failing work of {@link AuctionManager}
     */
    @Test
    public void testRandomBids() throws Exception {
        int initialQU = 20;
        int initialMU = 100;
        // Check multiply time for certainty
        for (int j = 0; j < 100; j++) {
            Random randomizer = new Random();
            BidderImpl bidder = new BidderImpl();
            AuctionManager managerMock = PowerMockito.spy(new AuctionManager(bidder, initialMU, initialQU));
            bidder.init(initialQU, initialMU);

            // Give a random ourBid on each step
            doAnswer(invocation -> randomizer.nextInt(initialMU)).when(managerMock, "readExternalBid");

            managerMock.startAuctionManagement();
        }
    }

    /**
     * Test auction with dummy external bids.
     * Check that our bidder is winner
     */
    @Test
    public void testZeroExternalBid() throws Exception {

        int initialQU = 20;
        int initialMU = 100;
        // Check multiply time for certainty
        for (int j = 0; j < 100; j++) {
            // Check with different easy to challenge external bids
            for (int i = 0; i < 2; i++) {
                ByteArrayOutputStream outContent = new ByteArrayOutputStream();

                System.setOut(new PrintStream(outContent));

                BidderImpl bidder = new BidderImpl();
                AuctionManager managerMock = PowerMockito.spy(new AuctionManager(bidder, initialMU, initialQU));
                bidder.init(initialQU, initialMU);

                // Give a random ourBid on each step
                doReturn(i).when(managerMock, "readExternalBid");

                managerMock.startAuctionManagement();

                assertThat(outContent.toString(), containsString(AuctionManager.OUR_BIDDER_WINNER_MESSAGE));

            }
        }
        System.setOut(System.out);
    }

    /**
     * Test quit
     */
    @Test
    public void testQuit() {

        int initialQU = 20;
        int initialMU = 100;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));

        ByteArrayInputStream in = new ByteArrayInputStream("quit".getBytes());
        System.setIn(in);

        BidderImpl bidder = new BidderImpl();
        AuctionManager managerMock = new AuctionManager(bidder, initialMU, initialQU);
        bidder.init(initialQU, initialMU);
        managerMock.startAuctionManagement();

        assertThat(outContent.toString(), containsString(AuctionManager.USER_DECIDED_TO_QUIT));

        System.setIn(System.in);

    }
}
